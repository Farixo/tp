# Maîtrise de poste

## Host OS
C:\Windows\system32>dxdiag

* LAPTOP-D1HQMJ9S
* Windows 10 Famille (10.0, version 18363)
    * 64 bits
    * 8193MB RAM, Micron Technology DDR4 8GB SODIMM 2666MTPS 1.2V, PartNumber=4ATS1G64HZ-2G6E1


## Devices

* Intel(R) Core(TM) i5-10210U CPU @ 1.60Ghz (8CPUs), ~2.1Ghz
    * 4 Coeurs
    * U : Mobile power efficient
* HID_DEVICE_SYSTEM_MOUSE
* NVIDIA GeForce MX110

## Disque Dur
```
    * Intel SSDPEKNW512G8
    * N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     C   OS           NTFS   Partition    475 G   Sain       Démarrag
  Volume 1         SYSTEM       FAT32  Partition    260 M   Sain       Système
  ```
* Function des Partition 
    * Une partition de démarrage permet à l'utilisateur d'installer plusieurs systèmes d'exploitation dans son ordinateur sans affecter les autres.
    * La partition système EFI est une partition sur un périphérique de stockage de données qui est utilisée par les ordinateurs adhérant à l'interface de micrologiciel extensible unifié.

# Users

```
* C:\Windows\system32>net user

comptes d’utilisateurs de \\LAPTOP-D1HQMJ9S

------------------------------------------------
Administrateur           DefaultAccount           farix
Invité                   WDAGUtilityAccount
```
# Processus

```
* C:\Windows\system32>tasklist

**Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation**

=============================================================================
System Idle Process              0 Services                   0         8 Ko
System                           4 Services                   0     3 728 Ko
Registry                       120 Services                   0    38 620 Ko
smss.exe                       496 Services                   0       932 Ko
csrss.exe                      640 Services                   0     4 788 Ko
wininit.exe                    828 Services                   0     5 532 Ko
services.exe                   904 Services                   0     9 028 Ko
lsass.exe                      912 Services                   0    17 652 Ko
svchost.exe                     84 Services                   0     2 864 Ko
svchost.exe                    528 Services                   0    30 864 Ko
fontdrvhost.exe                  8 Services                   0     2 152 Ko
svchost.exe                   1084 Services                   0    15 580 Ko
WUDFHost.exe                  1188 Services                   0     5 820 Ko
svchost.exe                   1208 Services                   0     7 112 Ko
WUDFHost.exe                  1404 Services                   0    15 156 Ko
svchost.exe                   1448 Services                   0     8 364 Ko
svchost.exe                   1456 Services                   0     9 616 Ko
svchost.exe                   1524 Services                   0     7 008 Ko
svchost.exe                   1544 Services                   0     4 696 Ko
svchost.exe                   1708 Services                   0     8 328 Ko
svchost.exe                   1832 Services                   0     8 980 Ko
svchost.exe                   1868 Services                   0    14 196 Ko
svchost.exe                   1952 Services                   0    15 956 Ko
svchost.exe                   1960 Services                   0     8 828 Ko
svchost.exe                   2000 Services                   0     6 212 Ko
NVDisplay.Container.exe       1196 Services                   0    13 268 Ko
svchost.exe                   2068 Services                   0    10 048 Ko
svchost.exe                   2084 Services                   0     7 336 Ko
svchost.exe                   2124 Services                   0    12 804 Ko
svchost.exe                   2256 Services                   0    10 820 Ko
svchost.exe                   2264 Services                   0     9 892 Ko
svchost.exe                   2272 Services                   0     4 332 Ko
svchost.exe                   2280 Services                   0     6 068 Ko
wsc_proxy.exe                 2320 Services                   0    10 648 Ko
svchost.exe                   2420 Services                   0     7 368 Ko
Memory Compression            2492 Services                   0   339 748 Ko
svchost.exe                   2536 Services                   0     7 592 Ko
igfxCUIService.exe            2660 Services                   0     6 344 Ko
taskhostw.exe                 2712 Services                   0    39 992 Ko
svchost.exe                   2796 Services                   0     6 740 Ko
svchost.exe                   2804 Services                   0     5 764 Ko
svchost.exe                   2820 Services                   0     8 796 Ko
svchost.exe                   3028 Services                   0    12 508 Ko
svchost.exe                   3160 Services                   0     4 932 Ko
svchost.exe                   3164 Services                   0     7 696 Ko
svchost.exe                   3344 Services                   0    15 856 Ko
svchost.exe                   3384 Services                   0    11 968 Ko
AsusOptimization.exe          3392 Services                   0     5 120 Ko
AvastSvc.exe                  3400 Services                   0   107 812 Ko
wlanext.exe                   3592 Services                   0     4 996 Ko
conhost.exe                   3600 Services                   0     4 120 Ko
svchost.exe                   3720 Services                   0     4 668 Ko
Intel_PIE_Service.exe         3784 Services                   0     5 960 Ko
svchost.exe                   3924 Services                   0     5 344 Ko
svchost.exe                   4000 Services                   0     5 016 Ko
aswToolsSvc.exe               4088 Services                   0    52 316 Ko
svchost.exe                   3756 Services                   0    17 224 Ko
WmiPrvSE.exe                  4188 Services                   0     8 812 Ko
svchost.exe                   4320 Services                   0     9 104 Ko
spoolsv.exe                   4348 Services                   0     9 032 Ko
svchost.exe                   4404 Services                   0    11 420 Ko
svchost.exe                   4432 Services                   0     5 740 Ko
svchost.exe                   4460 Services                   0    12 124 Ko
svchost.exe                   4732 Services                   0     5 744 Ko
AsusSystemAnalysis.exe        4900 Services                   0    10 500 Ko
AsusSoftwareManager.exe       4916 Services                   0    17 300 Ko
svchost.exe                   4932 Services                   0    19 120 Ko
ELANFPService.exe             4940 Services                   0     4 788 Ko
svchost.exe                   4924 Services                   0    18 572 Ko
AsusSystemDiagnosis.exe       4948 Services                   0     6 324 Ko
svchost.exe                   4956 Services                   0    27 160 Ko
OneApp.IGCC.WinService.ex     4964 Services                   0    17 464 Ko
svchost.exe                   4972 Services                   0    34 644 Ko
IntelCpHDCPSvc.exe            4980 Services                   0     5 412 Ko
AsusLinkNearExt.exe           4988 Services                   0     2 404 Ko
AsusLinkRemote.exe            4996 Services                   0    12 272 Ko
ibtsiva.exe                   5004 Services                   0     3 436 Ko
svchost.exe                   5012 Services                   0    12 720 Ko
IntelAudioService.exe         5020 Services                   0    12 688 Ko
svchost.exe                   5028 Services                   0     5 868 Ko
ICEsoundService64.exe         5036 Services                   0     5 352 Ko
RtkAudUService64.exe          5052 Services                   0     7 336 Ko
svchost.exe                   5060 Services                   0     5 976 Ko
AsusLinkNear.exe              5068 Services                   0     7 176 Ko
svchost.exe                   5076 Services                   0     4 124 Ko
RstMwService.exe              5084 Services                   0     5 116 Ko
svchost.exe                   5100 Services                   0     4 368 Ko
OfficeClickToRun.exe          5108 Services                   0    32 412 Ko
esif_uf.exe                   4640 Services                   0     4 204 Ko
svchost.exe                   5444 Services                   0     3 848 Ko
IntelCpHeciSvc.exe            5696 Services                   0     4 848 Ko
jhi_service.exe               5772 Services                   0     4 044 Ko
svchost.exe                   5904 Services                   0     6 488 Ko
svchost.exe                   6260 Services                   0     8 424 Ko
svchost.exe                   6252 Services                   0     7 796 Ko
aswidsagent.exe               4580 Services                   0   142 612 Ko
unsecapp.exe                  7420 Services                   0     7 732 Ko
dllhost.exe                   7568 Services                   0     7 828 Ko
PresentationFontCache.exe     4836 Services                   0    10 112 Ko
SearchIndexer.exe             2232 Services                   0    39 900 Ko
svchost.exe                   1388 Services                   0    19 208 Ko
svchost.exe                   3728 Services                   0     6 324 Ko
svchost.exe                   8248 Services                   0    17 016 Ko
svchost.exe                   8572 Services                   0     6 684 Ko
svchost.exe                   9416 Services                   0    15 652 Ko
SecurityHealthService.exe     9664 Services                   0    11 692 Ko
svchost.exe                  10520 Services                   0     8 528 Ko
svchost.exe                   8536 Services                   0    10 344 Ko
svchost.exe                   9872 Services                   0    12 516 Ko
svchost.exe                   8584 Services                   0    17 028 Ko
svchost.exe                   4332 Services                   0     5 284 Ko
svchost.exe                   6644 Services                   0    16 160 Ko
svchost.exe                   5628 Services                   0     9 544 Ko
SgrmBroker.exe                1692 Services                   0     6 088 Ko
svchost.exe                   8488 Services                   0    10 796 Ko
svchost.exe                   6784 Services                   0    14 876 Ko
svchost.exe                   6816 Services                   0     6 336 Ko
svchost.exe                  15264 Services                   0     6 396 Ko
svchost.exe                  14380 Services                   0     6 696 Ko
svchost.exe                   9000 Services                   0     9 100 Ko
svchost.exe                    844 Services                   0     5 348 Ko
svchost.exe                   8808 Services                   0     7 936 Ko
GoogleCrashHandler.exe        4268 Services                   0     1 120 Ko
GoogleCrashHandler64.exe     12860 Services                   0     1 012 Ko
svchost.exe                  11236 Services                   0     4 804 Ko
svchost.exe                  12688 Services                   0     7 952 Ko
csrss.exe                     8912                            2     6 368 Ko
svchost.exe                  16016 Services                   0     7 668 Ko
bash                         13140                            2     2 840 Ko
svchost.exe                  11748 Services                   0     6 916 Ko
svchost.exe                  16080 Services                   0     6 164 Ko
audiodg.exe                  17976 Services                   0    61 896 Ko
csrss.exe                    11628 Console                    3     5 540 Ko
winlogon.exe                 18096 Console                    3     9 464 Ko
fontdrvhost.exe              13708 Console                    3     6 904 Ko
dwm.exe                       3216 Console                    3    66 412 Ko
NVDisplay.Container.exe      11128 Console                    3    31 180 Ko
svchost.exe                   2632 Services                   0     4 856 Ko
AsusOptimizationStartupTa    13944 Console                    3    12 864 Ko
sihost.exe                   13348 Console                    3    26 560 Ko
svchost.exe                  17216 Console                    3    24 656 Ko
svchost.exe                   8804 Console                    3     7 616 Ko
igfxEM.exe                    5668 Console                    3    15 396 Ko
svchost.exe                  14092 Console                    3    28 196 Ko
taskhostw.exe                15728 Console                    3    17 816 Ko
ctfmon.exe                   16888 Console                    3    13 256 Ko
explorer.exe                  9788 Console                    3   120 728 Ko
svchost.exe                  18248 Services                   0     7 176 Ko
svchost.exe                   4832 Console                    3    21 636 Ko
StartMenuExperienceHost.e     9224 Console                    3    60 348 Ko
RuntimeBroker.exe            16788 Console                    3    23 392 Ko
SearchUI.exe                  8440 Console                    3   196 408 Ko
RuntimeBroker.exe            18624 Console                    3    22 052 Ko
LockApp.exe                  18532 Console                    3    40 708 Ko
RuntimeBroker.exe            11584 Console                    3    27 760 Ko
YourPhone.exe                16500 Console                    3       180 Ko
RuntimeBroker.exe            10920 Console                    3    15 608 Ko
SettingSyncHost.exe          15332 Console                    3     6 368 Ko
RuntimeBroker.exe            15328 Console                    3    18 236 Ko
svchost.exe                  13072 Console                    3    20 028 Ko
SecurityHealthSystray.exe    10340 Console                    3     8 188 Ko
AvastUI.exe                   9984 Console                    3    39 068 Ko
vgtray.exe                    9040 Console                    3     6 484 Ko
IGCCTray.exe                  1920 Console                    3    55 072 Ko
IGCC.exe                      4252 Console                    3    39 092 Ko
Spotify.exe                  17804 Console                    3    90 536 Ko
Spotify.exe                   8660 Console                    3    13 920 Ko
jusched.exe                  12016 Console                    3     6 460 Ko
Spotify.exe                  19364 Console                    3    60 448 Ko
Spotify.exe                  10224 Console                    3    29 380 Ko
Spotify.exe                  10424 Console                    3   143 704 Ko
RtkAudUService64.exe          6552 Console                    3     1 480 Ko
AvastUI.exe                   6416 Console                    3     1 880 Ko
AvastUI.exe                   7956 Console                    3     2 248 Ko
AsusOSD.exe                  16352 Console                    3    11 468 Ko
AsusSoftwareManagerAgent.    18460 Console                    3    25 076 Ko
ShellExperienceHost.exe       2388 Console                    3    41 632 Ko
RuntimeBroker.exe            14072 Console                    3    13 588 Ko
WINWORD.EXE                   9372 Console                    3   167 468 Ko
cmd.exe                       1040 Console                    3     5 048 Ko
conhost.exe                   5280 Console                    3    13 188 Ko
vds.exe                      10136 Services                   0    12 528 Ko
opera.exe                    11076 Console                    3   169 872 Ko
opera_crashreporter.exe      13088 Console                    3     7 184 Ko
opera.exe                     2100 Console                    3   186 592 Ko
opera.exe                     8480 Console                    3    44 492 Ko
opera.exe                     3412 Console                    3    19 136 Ko
opera.exe                     3516 Console                    3    47 256 Ko
opera.exe                    13656 Console                    3    18 468 Ko
opera.exe                     1968 Console                    3    88 408 Ko
opera.exe                     5136 Console                    3   185 988 Ko
opera.exe                    11600 Console                    3    71 904 Ko
opera.exe                    10736 Console                    3   139 036 Ko
opera.exe                    14016 Console                    3    62 092 Ko
opera.exe                    12676 Console                    3    55 416 Ko
opera.exe                    17872 Console                    3    55 608 Ko
opera.exe                     5960 Console                    3    69 880 Ko
opera.exe                    18760 Console                    3    57 208 Ko
opera.exe                    10400 Console                    3    59 412 Ko
opera.exe                    13680 Console                    3    18 668 Ko
opera.exe                     4372 Console                    3   153 788 Ko
opera.exe                     3916 Console                    3   111 540 Ko
opera.exe                    11624 Console                    3    87 584 Ko
opera.exe                    17052 Console                    3    43 768 Ko
opera.exe                     6084 Console                    3    17 304 Ko
opera.exe                    12544 Console                    3    72 992 Ko
opera.exe                     9476 Console                    3    52 312 Ko
WindowsInternal.Composabl    11732 Console                    3    42 324 Ko
RuntimeBroker.exe             2096 Console                    3     5 804 Ko
ApplicationFrameHost.exe     15960 Console                    3    23 460 Ko
rundll32.exe                 17048 Console                    3     8 908 Ko
dllhost.exe                  16576 Console                    3    12 556 Ko
opera.exe                    16212 Console                    3    29 432 Ko
SearchProtocolHost.exe       12920 Console                    3     8 080 Ko
tasklist.exe                  9600 Console                    3    10 316 Ko
WmiPrvSE.exe                 17568 Services                   0     8 984 Ko
```
---
    * svchost.exe                     84 Services                   0     2 864 Ko
    Il sert d'hôte pour les fonctionnalités de bibliothèques de liens dynamiques.
    
---
    * csrss.exe                    11628 Console                    3     5 540 Ko
    Le sous - système d'exécution du client, ou csrss.exe, est un composant de la famille de systèmes d'exploitation Windows NT qui fournit le côté utilisateur du sous - système Win32 en mode utilisateur. Il est inclus dans Windows NT 3.1 et les versions ultérieures.
    
---
    * explorer.exe                  9788 Console                    3   120 728 Ko
    Le gestionnaire permet, notamment, d'afficher et de modifier le nom des fichiers et des dossiers, de manipuler les fichiers et les dossiers, d'ouvrir les fichiers de données, et de lancer les programmes.
    
---
    * smss.exe                       496 Services                   0       932 Ko
    Il lance autochk.exe pour vérifier le ou les différent systèmes de fichiers, puis après cette vérification, il crée les variables d'environnement et démarre.
    
---
    * lsass.exe                      912 Services                   0    17 652 Ko
    Il assure l'identification des utilisateurs.

* Il faut taper dans l’invite de commande taskmgr qui ouvrira le gestionnaire de tâche -> Détails -> Sélectionner des colonnes -> Elever 

# Network
```
* PS C:\Users\farix> Get-NetAdapter
Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Connexion réseau Bluet... Bluetooth Device (Personal Area Netw...      16 Disconnected 08-D2-3E-62-C0-E0         3 Mbps
Wi-Fi                     Intel(R) Wireless-AC 9462                    11 Up           08-D2-3E-62-C0-DC       200 Mbps
VirtualBox Host-Only N... VirtualBox Host-Only Ethernet Adapter         7 Up           0A-00-27-00-00-07         1 Gbps
```
* TCP:

```
C:\Windows\system32>netstat -p tcp -a -n -b

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING
  RpcEptMapper
 [svchost.exe]
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:808            0.0.0.0:0              LISTENING
 [OneApp.IGCC.WinService.exe]
  TCP    0.0.0.0:5040           0.0.0.0:0              LISTENING
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:7680           0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:9001           0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING
 [lsass.exe]
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING
 [spoolsv.exe]
  TCP    0.0.0.0:49670          0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:57621          0.0.0.0:0              LISTENING
 [Spotify.exe]
  TCP    0.0.0.0:58759          0.0.0.0:0              LISTENING
 [Spotify.exe]
  TCP    127.0.0.1:12025        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12110        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12119        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12143        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12465        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12563        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12993        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12995        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:27275        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:139       0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:58725     40.67.254.36:443       ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    192.168.1.50:58770     35.190.242.206:4070    ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:58775     35.186.224.47:443      ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:58808     5.62.53.11:80          ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:58859     77.234.45.64:80        ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:59198     91.121.38.247:443      ESTABLISHED
 [opera.exe]
  TCP    192.168.1.50:59286     92.222.75.23:443       ESTABLISHED
 [opera.exe]
  TCP    192.168.1.50:59624     69.173.144.158:443     TIME_WAIT
  TCP    192.168.1.50:59631     151.101.122.49:443     ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59653     51.104.167.255:443     TIME_WAIT
  TCP    192.168.1.50:59658     20.54.24.246:443       ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:59659     51.104.167.255:443     ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:59660     51.104.167.255:443     ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:59661     51.104.167.255:443     ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:59665     213.19.162.21:443      ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59666     52.109.68.21:443       TIME_WAIT
  TCP    192.168.1.50:59667     213.19.162.57:443      ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59668     52.109.68.21:443       TIME_WAIT
  TCP    192.168.1.50:59670     37.157.2.234:443       ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59671     69.173.144.139:443     ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59672     35.244.147.96:443      ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59673     52.28.82.26:443        ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59674     52.57.167.187:443      ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59675     35.186.193.173:443     ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59676     35.156.158.150:443     ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59677     23.57.5.124:443        ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59678     85.114.159.93:443      ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59679     69.173.144.139:443     ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59680     52.19.189.90:443       ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59681     10.33.19.123:7680      SYN_SENT
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:59682     10.33.18.87:7680       SYN_SENT
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.56.1:139       0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
 ```
 * Fonction TCP : 
     * WinStore.App.exe : Windows Store
     * svchost.exe :Il sert d'hôte pour les fonctionnalités de bibliothèques de liens 
     * opera.exe : Navigateur OperaGX
     * Discord.exe : Plateforme de communication
     * SystemSettings.exe : Paramètres PC de Windows
     * SearchUI.exe : SearchUI.exe active l'interface utilisateur de recherche de l'assistant de recherche Microsoft Cortana. 
 
 * UDP : 
```
C:\Windows\system32>netstat -p udp -a -n -b

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  UDP    0.0.0.0:123            *:*
  W32Time
 [svchost.exe]
  UDP    0.0.0.0:500            *:*
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:4500           *:*
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:5050           *:*
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5355           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:50739          *:*
 [opera.exe]
  UDP    0.0.0.0:52660          *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    0.0.0.0:54196          *:*
 [opera.exe]
  UDP    0.0.0.0:57412          *:*
 [Spotify.exe]
  UDP    0.0.0.0:57413          *:*
 [Spotify.exe]
  UDP    0.0.0.0:57621          *:*
 [Spotify.exe]
  UDP    127.0.0.1:1900         *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:52656        *:*
  iphlpsvc
 [svchost.exe]
  UDP    127.0.0.1:52658        *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    127.0.0.1:53698        *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:63132        *:*
 [WINWORD.EXE]
  UDP    192.168.1.50:137       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.1.50:138       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.1.50:1900      *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.1.50:2177      *:*
  QWAVE
 [svchost.exe]
  UDP    192.168.1.50:53697     *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.56.1:137       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.56.1:138       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.56.1:1900      *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.56.1:2177      *:*
  QWAVE
 [svchost.exe]
  UDP    192.168.56.1:53696     *:*
  SSDPSRV
 [svchost.exe]
 ```
 * Function UDP : 
     * svchost.exe : Il sert d'hôte pour les fonctionnalités de bibliothèques de liens dynamiques.
     * opera.exe : Navigateur OperaGX
     * WINWORD.EXE : logiciel de traitement de texte

# Scripting

```
Auteur : De Freitas Flavio
Date : 25/10/2020

Write-Output "Nom de l'ordinateur : $env:computername"
$Os = (Get-WMIObject win32_operatingsystem).name
$Os = $Os.split("|")[0]
Write-Output "Os : $Os"
$OSVersion = (Get-WMIObject win32_operatingsystem).version
Write-Output "Os Version : $OsVersion"
$Date = (Get-CimInstance Win32_OperatingSystem).LastBootUpTime 
Write-Output "Date et heure d'allumage : $Date"
$criteria = "Type='software' and IsAssigned=1 and IsHidden=0 and IsInstalled=0"
$searcher = (New-Object -COM Microsoft.Update.Session).CreateUpdateSearcher()
$updates = $searcher.Search($criteria).Updates
if ($updates.Count -ne 0) {
    $osUpdated = "Le systeme est t'il a jour : Non"
}
else {
    $osUpdated = "Le systeme est t'il a jour : Oui"
}
Write-Output "Est-ce que l'Os est a jour : $osUpdated"

Write-Output " "

$RamTotale = [STRING]((Get-WmiObject -Class Win32_ComputerSystem ).TotalPhysicalMemory / 1GB)
Write-Output "Ram totale : $RamTotale"
$RamLibre = (Get-CIMInstance Win32_OperatingSystem).FreePhysicalMemory
Write-Output "RAM disponible : $RamLibre Go"
$RamUtiliser = [String]((Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory / 1MB)
Write-Output "Ram utiliser : $RamUtiliser Go"
$DiskTotal = [Math]::Round((Get-Volume -DriveLetter 'C').Size / 1GB)
$DiskUse = [Math]::Round((Get-Volume -DriveLetter 'C').SizeRemaining / 1GB)
Write-Output "Espace disque utiliser : $DiskUse Go"
$DiskDispo = ($DiskTotal - $DiskUse)
Write-Output "Espace disque disponible : $DiskDispo Go"

Write-Output " "

$ip = (Test-Connection -ComputerName $env:computername -count 1).IPV4Address.ipaddressTOstring
Write-Output "Ip principale : $ip"
$connection = (Test-Connection -ComputerName "8.8.8.8" -Count 4  | measure-Object -Property ResponseTime -Average).average
Write-Output " - Ping : $connection ms"
$DownloadSpeed = [math]::Round($SpeedtestResults.download.bandwidth / 1000000 * 8, 2)
$UploadSpeed = [math]::Round($SpeedtestResults.upload.bandwidth / 1000000 * 8, 2)
Write-Output " - download speed : $DownloadSpeed Mbit/s"
Write-Output " - upload speed : $UploadSpeed Mbit/s"

Write-Output " "

$Users = (Get-WmiObject Win32_UserAccount).Name 
Write-Output "La liste des utilisateurs : $Users"

echo "Lock le PC au bout de 30 seconde"
Start-Sleep -Seconds 30
rundll32.exe user32.dll, LockWorkStation

echo "Eteins le PC au bout de 75 seconde"
Start-Sleep -Seconds 75
shutdown.exe /s
```

# Gestion de softs

Un gestionnnaire de paquets sert à automatiser l'installation et la désinstallation de logiciels. Tout est beaucoup plus rapide, les paquets ont vérifiés et donc dépourvus de logiciels malveilants.

Liste des paquets : 
```
PS C:\Users\DIRECTEUR_PC2> choco list -l
Chocolatey v0.10.15
chocolatey 0.10.15
1 packages installed.
```

Provenance des paquets : 
```
PS C:\Users\DIRECTEUR_PC2> choco source
Chocolatey v0.10.15
chocolatey - https://chocolatey.org/api/v2/ | Priority 0|Bypass Proxy - False|Self-Service - False|Admin Only - False.
```
# Partage de fichiers

```PS C:\Users\farix> ssh root@192.168.56.102
root@192.168.56.102's password:
Last login: Mon Nov  9 16:34:29 2020
[root@localhost ~]#

[root@localhost opt]# ls
partage
[root@localhost opt]# cd partage/
[root@localhost partage]# ls
share.txt
```
